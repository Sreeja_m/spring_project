package com.epam.dto.response;

import java.time.LocalDate;
import java.util.List;

import com.epam.entity.Trainee;
import com.epam.entity.Trainer;
import com.epam.entity.Training;
import com.epam.entity.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrainerProfile {
	private String trainerUsername;
	private String trainerFirstName;
	private String trainerLastName;
	private String trainerSpecialization;
	private boolean isActive;
	private List<TraineeDto> traineeList; 

}
